import PySimpleGUI as sg

sg.theme('BluePurple')

device_layout = [
    [sg.Text('Enter commercialType')], [sg.InputText('PL', key='-COMMERCIAL_TYPE-')],
    [sg.Text('Enter deviceId')], [sg.InputText('', key='-DEVICE_ID-')],
    [sg.Button('Select deviceId')],
    [sg.Text(size=(20, 1), key='-COMPLETE_DEVICE_ID-')],
    [sg.Text('Select assetMake'), sg.Text(size=(15, 1), key='-HINT-')],
    [sg.Radio('', "MAKE_RADIO", key='-RADIO1-', default=True)],
    [sg.Radio('', "MAKE_RADIO", key='-RADIO2-')],
    [sg.Button('Select')],
    [sg.Text('Select assetSerialNumber')], [sg.InputText('', key='-SERIAL_NUMBER-')],
    [sg.Button('Submit'), sg.Button('Exit')]
]

window = sg.Window('deviceType Window', device_layout)
while True:  # Event Loop
    event, values = window.read()
    print(event, values)
    if event in (None, 'Exit'):
        break
    elif values['-DEVICE_ID-']:
        updated_deviceId = 'Cinco-' + values['-COMMERCIAL_TYPE-'] + '-GXY-' + values['-DEVICE_ID-']
        window['-COMPLETE_DEVICE_ID-'].update(updated_deviceId)
    if event in (None, 'Exit'):
        break
    elif values['-RADIO1-']:
        updated_serialNumber = 'GXY' + updated_deviceId[-5:]
        window['-SERIAL_NUMBER-'].update(updated_serialNumber)
    elif values['-RADIO2-']:
        updated_serialNumber = '20' + values['-COMMERCIAL_TYPE-'][-3:] + updated_deviceId[-5:]
        window['-SERIAL_NUMBER-'].update(updated_serialNumber)
