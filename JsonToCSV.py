import json
from pydoc import plain
from csv import writer

#Reads the raw log file
with open("C:/Users/Karthick Srinivasan/Downloads/sample.txt",'r+') as sampleread:
    for line in sampleread:
        if '{' in line:
            data = sampleread.readlines()

#Filepath to write the content in proper json format
filetowrite = "C:/Users/Karthick Srinivasan/Downloads/sample1.txt"

#Putting all the json objects in a json array
with open(filetowrite,'w') as samplewrite:
    samplewrite.writelines('{"samplejson":[{')
    samplewrite.writelines(data)

#Removing the connection log statements
with open(filetowrite,'r+') as samplewrite:
    data=samplewrite.readlines()[:-1]
    samplewrite.seek(0)
    samplewrite.truncate()
    samplewrite.writelines(data)
    samplewrite.writelines(']}')

#Separating all the json objects with a comma
with open(filetowrite,'r+') as samplewrite:
    lines = samplewrite.readlines()
    for i in range(len(lines)):
        if lines[i] == '}\n' and lines[i+1] == '{\n':
            lines[i]=lines[i].replace('}\n','},\n')
    samplewrite.seek(0)
    samplewrite.truncate()
    samplewrite.writelines(lines)

#Function to flatten the json
def flatten_json(y):
	out = {}

	def flatten(x, name =''):
		
		# If the Nested key-value
		# pair is of dict type
		if type(x) is dict:
			
			for a in x:
				flatten(x[a], name + a + '_')
				
		# If the Nested key-value
		# pair is of list type
		elif type(x) is list:
			
			i = 0
			
			for a in x:				
				flatten(a, name + str(i) + '_')
				i += 1
		else:
			out[name[:-1]] = x

	flatten(y)
	return out

def read_json(filename: str) -> dict:
  
    try:
        with open(filename, "r") as f:
            data = json.loads(f.read())
    except:
        raise Exception(f"Reading {filename} file encountered an error")
  
    return data

def normalize_json(data: dict) -> dict:
  
    new_data = dict()
    for key, value in data.items():
        if not isinstance(value, dict):
            new_data[key] = value
        else:
            for k, v in value.items():
                new_data[key + "_" + k] = v
  
    return new_data
  
#Function to generate CSV  
def generate_csv_data(data: dict) -> str:
  
    # Defining CSV columns in a list to maintain
    # the order
    csv_columns = data.keys()
  
    # Generate the first row of CSV 
    csv_data = ",".join(csv_columns) + "\n"
  
    # Generate the single record present
    new_row = list()
    for col in csv_columns:
        new_row.append(str(data[col]))
  
    # Concatenate the record with the column information 
    # in CSV format
    csv_data += ",".join(new_row) + "\n"
  
    return csv_data
  
#Function to write to CSV file
def write_to_file(data: str, filepath: str) -> bool:
  
    try:
        with open(filepath, "w+") as f:
            f.write(data)
    except:
        raise Exception(f"Saving data to {filepath} encountered an error")


def main():
    output_list=[]
    f = open(filetowrite)
    data = json.load(f)
    for i in data['samplejson']:
        csv_data = generate_csv_data(data=flatten_json(i))
        output_list.append(csv_data)
    f= open('data.csv', 'w') 
    for i in output_list:
        print('output '+i)
        f.write(i)
        
    
if __name__ == '__main__':
    main()

 


